FROM debian:bookworm-slim
ARG VERSION=0.14.1

RUN apt-get update && apt-get install -y --no-install-recommends \
    biber \
    ca-certificates \
    fonts-lato \
    fonts-roboto \
    fonts-roboto-slab \
    texlive-binaries \
    && rm -rf /var/lib/apt/lists/*
ADD "http://archive.ubuntu.com/ubuntu/pool/universe/b/biber/biber_2.17-2_all.deb" .
RUN dpkg --install "biber_2.17-2_all.deb"

ADD "https://github.com/tectonic-typesetting/tectonic/releases/download/tectonic%40${VERSION}/tectonic-${VERSION}-x86_64-unknown-linux-musl.tar.gz" .
RUN tar --extract --directory="/usr/bin/" --file="tectonic-${VERSION}-x86_64-unknown-linux-musl.tar.gz"
